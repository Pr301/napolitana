var Cameriera = new Class({


	initialize: function() {
		var self = this;

		this.date     = new Date();

		new Request.JSON({
			url: base_url + "/get_napolitana.php",
			onSuccess: function(data) {
				self.nap = new Napolitana({
					target:      "main",
					data:        data,
					user_select: true,
					onStart: function(event) {
						self.take_order(event.name, event.date);
					}
				});
			},
		}).get();
	},

	take_order: function(name, date) {
		new Request.JSON({
			url: base_url + "/add_pomodoro.php",
		}).post({"name": name, "date": date.toJSON()});
	},

});
