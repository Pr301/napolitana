var Napolitana = new Class({

	/*

	options

		data:   JSON Object

		target: Element or element id for the Napolitana wrapper

		names:  Array of names to show the start button.
				If not defined, it will show on all of them

		show:   boolean (default false)
				show the napolitana on init.

		user_select:    boolean (default true).
						adds screen to select a user. disables show.

	*/


	Implements: [Options, Events],

	initialize: function(options) {
		this.setOptions(options);
		if(typeof(this.options.data) != "undefined")
			this.set_data(this.options.data);
		if(!(this.options.target instanceof Element))
			this.options.target = $(this.options.target);
		if(typeof(this.options.user_select) == "undefined")
			this.options.user_select = false;
		if(typeof(this.options.show) == "undefined" || this.options.user_select)
			this.options.show = false;

		this.pomodori = new Array();

		if(this.options.show) {
			this.show();
		} else if(this.options.user_select) {
			this.libro_doro(this.get_names);
		}

	},

	// Getters/Setters

	set_data: function(data) {
		this.options.data = data.napolitana.sort(function(x,y) { return ((x.user == y.user) ? 0 : ((x.user > y.user) ? 1 : -1 )) });
	},

	set_names: function(names) {
		this.options.names = names;
	},

	num_users: function() {
		return this.options.data.length;
	},

	get_names: function() {
		var names = new Array();
		for (var i = 0; i < this.options.data.length; i++) {
			names.push(this.options.data[i].user);
		}
		names.sort();
		return names;
	},

	// Pomodoro Handling

	show: function() {
		this._draw_table();
		this.resume_pomodori();
	},

	pomodoro: function(name, date) {
		if(typeof(date) == "undefined") date = new Date();
		if(!(date instanceof Date))     date = new Date(this.options.date);

		var self = this;
		var pom = new Pomodoro({
			name: name,
			onComplete: function() {
				self.pomodori.splice(self.pomodori.indexOf(pom), 1);
			}
		});
		pom.start();
		this.pomodori.push(pom);

		for(var i = 0; i < this.options.data.length; i++) {
			if(this.options.data[i].user == name) {
				this.options.data[i].pomodori.push({"name": name, "date": date.toJSON()});
			}
		}

		this._reset_counters();
	},

	start: function(name) {
		var d = new Date();
		this.pomodoro(name, d);
		this.fireEvent('start', {"name": name, "date": d});
	},

	is_active: function(name) {
		this.pomodori.forEach(function(pom){
			if(pom.name == name) return true;
		});
		return false;
	},

	_name_exists: function(name) {
		if(typeof(this.options.names) == "undefined") return true;
		for(var i = 0; i < this.options.names.length; i++) {
			if(this.options.names[i] == name) {
				return true;
			}
		}
		return false;
	},

	resume_pomodori: function() {
		var self = this;

		var thresshold   = new Date(new Date().getTime() - pom_duration * 60000);
		for(var i = 0; i < this.options.data.length; i++) {
			for(var j = 0; j < this.options.data[i].pomodori.length; j++) {
				var pom_date = new Date(this.options.data[i].pomodori[j].date);
				if(pom_date > thresshold && !this.is_active(this.options.data[i].user)) {
					var pom = new Pomodoro({
						name: this.options.data[i].user,
						date: this.options.data[i].pomodori[j].date,
						onComplete: function() {
							self.pomodori.splice(self.pomodori.indexOf(pom), 1);
						}
					});
					pom.start();
					this.pomodori.push(pom);
				}
			}
		}
		this._reset_counters();
	},

	_draw_table: function () {

		var pom_table = new HtmlTable({
			properties: {
				class: "rounded-corner"
			},
			headers: [
				"Name",
				"Pomodori",
				"Activity"
			]
		});

		var btn_det    = new Array();
		for(var i = 0; i < this.options.data.length; i++) {
			var r_name = new Element('span', {html: this.options.data[i].user});
			var r_pom  = this._get_pom_html_el(this.options.data[i].user);
			var b_id   = this.options.data[i].user + '_btn';
			var r_btn  = new Element('button', {
				id: b_id,
				html: 'Start!!',
				styles: {display: 'none'}
			});

			btn_det.push({"id": b_id, "name": this.options.data[i].user});

			pom_table.push([r_name.outerHTML, r_pom.innerHTML, r_btn.outerHTML], {
				"class": "inactive",
				"id": this.options.data[i].user
			});
		}

		this.options.target.innerHTML = "";
		pom_table.inject($(this.options.target));

		var self = this;
		for (var i = 0; i < btn_det.length; i++) {

			if(this._name_exists(btn_det[i].name)) {
				$(btn_det[i].id).set('styles', {display: 'block'});
			}
		}

		// TODO: find a workaround for this
		// if the addEvent isn't in a forEach then
		// on click the btn_det[i].name is undefined
		// if we do something like var det = btn_det[i] then
		// pomodoro only fires for the last person
		btn_det.forEach(function(det){
			$(det.id).addEvent('click', function() {self.start(det.name)});
		});
	},

	_get_pom_html_el: function(name) {
		var details  = [
			{
				"text": "Today: ",
				"short": "d"
			},
			{
				"text": "Week: ",
				"short": "w"
			},
			{
				"text": "Total: ",
				"short": "t"
			}
		];

		var div_el   = new Element('div');
		for(var i = 0; i < details.length; i++) {
			var count_el = new Element('span', {id: name + '_' + details[i].short, html: '0'});
			var b_el     = new Element('b');
			var ext_el   = new Element('span', {html: details[i].text});

			count_el.inject(b_el);
			b_el.inject(ext_el);
			ext_el.inject(div_el);

			if(i < details.length - 1) {
				var br_el    = new Element('br');
				br_el.inject(div_el);
			}
		}
		return div_el;
	},

	_reset_counters: function() {
		for(var i = 0; i < this.options.data.length; i++) {
			var name = this.options.data[i].user
			var t_el = $(name + "_t");
			var w_el = $(name + "_w");
			var d_el = $(name + "_d");

			t_el.innerHTML = this.get_total(name);
			w_el.innerHTML = this.get_week(name);
			d_el.innerHTML = this.get_day(name);
		}
	},

	// User select
	
	libro_doro: function() {
		if (this.load_from_cookie()) return;

		var ol     = new Element('ol', {'class': 'rounded-list'});
		var self   = this;
		var names  = this.get_names()
		for (var i = 0; i < names.length; i++) {
			var li = new Element('li');
			var a  = new Element('a', {
				id:   names[i],
				html: names[i],
				events: {
					click: function() {
						self.name_selected(this.id);
					}
				}
			});
			a.inject(li);
			li.inject(ol);
		}
		this.options.target.innerHTML = "";
		ol.inject(this.options.target);
	},

	name_selected: function(name) {
		Cookie.write('name', name);
		this.set_names([name]);
		this.show();
	},

	load_from_cookie: function() {
		var name = Cookie.read('name');
		if(  typeof(name) == "null" ||
			(typeof(name) == "object" && name == null) ||
			(typeof(name) == "string" && name == "null")) return false;
		this.name_selected(name);
		return true;
	},

	// Date Handling

	get_total: function(user) {
		for(var i = 0; i < this.options.data.length; i++) {
			if(this.options.data[i].user == user) {
				return this.options.data[i].pomodori.length;
			}
		}
		return 0;
	},

	get_week: function(user) {
		var pom = 0;

		for(var i = 0; i < this.options.data.length; i++) {
			if(this.options.data[i].user == user) {
				for(var p = 0; p < this.options.data[i].pomodori.length; p++) {
					var p_date = new Date(this.options.data[i].pomodori[p].date);
					if(p_date >= new Date().getWeekStart() && p_date < new Date().getWeekEnd()) {
						pom++;
					}
				}
				return pom;
			}
		}
		return pom;
	},

	get_day: function(user) {
		var today = new Date();
		today.resetTime();

		var pom = 0;
		for(var i = 0; i < this.options.data.length; i++) {
			if(this.options.data[i].user == user) {
				for(var p = 0; p < this.options.data[i].pomodori.length; p++) {
					var p_date = new Date(this.options.data[i].pomodori[p].date);
					p_date.resetTime();
					if(today - p_date === 0) {
						pom++;
					}
				}
				return pom;
			}
		}
		return pom;
	},

});
