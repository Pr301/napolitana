window.addEvent('domready', function() {
	var c = new Cameriera();
});

Date.prototype.mGetDay = function() {
	return (this.getDay() + 6) %7;
}

Date.prototype.getWeekStart = function() {
	var current = new Date();
	var weekstart = current.getDate() - current.mGetDay() + 1;
	var monday = new Date(current.setDate(weekstart));

	monday.resetTime();

	return monday;
}

Date.prototype.getWeekEnd = function() {
	var current = new Date();
	var weekend = current.getDate() - current.mGetDay() + 8;
	var sunday = new Date(current.setDate(weekend));

	sunday.resetTime();

	return sunday;
}

Date.prototype.resetTime = function() {
	this.setHours(0);
	this.setMinutes(0);
	this.setSeconds(0);
	this.setMilliseconds(0);
}
