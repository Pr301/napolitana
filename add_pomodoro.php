<?php
require_once("access_control.php");

$name = $_POST['name'];
$date = $_POST['date'];

$fn   = "data/napolitana.json";
$fp   = fopen($fn, 'r+');
$data = json_decode(fread($fp, filesize($fn)), true);
$nap  = $data['napolitana'];

for($i = 0; $i < count($nap); $i++) {
	if($name == $nap[$i]['user']) {
		$pom = $nap[$i]['pomodori'];
		array_push($pom, array("date" => $date, "active" => False));
		$nap[$i]['pomodori'] = $pom;
		$data['napolitana']  = $nap;
		break;
	}
}

ftruncate($fp, 0);
rewind($fp);
fwrite($fp, json_encode($data));
fclose($fp);

echo json_encode($data);


?>
