<?php
# Allow local origins
$allowed_origins = array(
						);

$http_origin = $_SERVER['HTTP_ORIGIN'];
if (in_array($http_origin, $allowed_origins)){
	header('Access-Control-Allow-Origin: '.$http_origin);
}

header('Access-Control-Allow-Headers: X-Requested-With, X-Request');
