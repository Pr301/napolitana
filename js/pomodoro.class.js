var Pomodoro = new Class({

	/*

	options: {
		name
		date
	}

	*/

	Implements: [Options, Events],

	initialize: function(options) {
		this.setOptions(options);
		if(typeof(this.options.date) == "undefined") this.options.date = new Date();
		if(!(this.options.date instanceof Date))     this.options.date = new Date(this.options.date);
		this.name = this.options.name;
		this.date = this.options.date;
	},

	get_name: function() {
		return this.options.name;
	},

	start: function(display_start) {
		if(typeof(display_start) == "undefined") display_start = true;

		var tr        = $(this.options.name);
		var btn       = $(this.options.name + "_btn");
		var td        = btn.parentNode;

		tr.removeClass("inactive");
		tr.addClass("active");
		btn.set('styles', {display: 'none'});

		var end     = new Date(this.options.date.getTime() + pom_duration * 60000);

		var c_id    = "counter_" + this.options.name;
		var counter = new Element("div", {"id": c_id, "class": "counter"});
		counter.inject(td);

		var div = $(c_id);
		var self = this;
		this.coundown = new CountDown({
			date: end,
			frequency: 100,
			onChange: function(counter) {
				var text = '';
				if(counter.days > 0) text = counter.days + ' d ';
				if(counter.hours > 0) text += (counter.hours >= 10 ? '' : '0') + counter.hours + ':';
				text += (counter.minutes >= 10 ? '' : '0') + counter.minutes + ':';
				text += (counter.second >= 10 ? '' : '0') + counter.second;
				div.set('text', text);
			},
			onComplete: function () {
				counter.destroy();
				if(display_start) btn.set('styles', {display: 'block'});
				tr.removeClass("active");
				tr.addClass("inactive");
				self.fireEvent("complete");
			}
		});
	},

	stop: function() {
		this.coundown.stop();
	}

});
